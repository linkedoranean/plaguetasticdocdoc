﻿using System;
using System.Collections;
using UnityEngine;
using TMPro;

public class TextManager : MonoBehaviour
{
    public enum ActionToTurnOn { none, closeAttack, rangeAttack };
    public ActionToTurnOn selectedAction;
    
    public bool closeIntro;

    [TextArea]
    public string[] texts;
    public int textCounter;

    public TextMeshProUGUI text;

    public GameObject gameManager;
    public InputManager inputManager;
    public PlayerManager playerManager;

    public Animator introAnim;

    void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController");
        inputManager = gameManager.GetComponent<InputManager>();
        playerManager = FindObjectOfType<PlayerManager>();
        introAnim = GetComponent<Animator>();
    }

    void Start()
    {
        closeIntro = true;
    }

    void OnEnable()
    {
        inputManager.enabled = false;
    }

    public void nextText()
    {
        if (texts[textCounter] == "#")
        {
            text.text = "";

            if (closeIntro)
            {
                introAnim.SetTrigger("HideLeft");
                StartCoroutine(SetAttackOn());
            }
        }

        if (texts[textCounter] != "#")
        {
            text.text = texts[textCounter];
            textCounter++;
        }
    }

    IEnumerator SetAttackOn()
    {
        closeIntro = false;
        yield return new WaitForSeconds(1f);


        if (playerManager.tutorial.selectedCanvas == playerManager.tutorial.introCanvas.Length)
        {
            playerManager.tutorial.showTutorial = false;
        }

        inputManager.enabled = true;   
        
        if (selectedAction == ActionToTurnOn.closeAttack)
        {
            inputManager.canCloseAttack = true;   
        }
        
        if (selectedAction == ActionToTurnOn.rangeAttack)
        {
            inputManager.canRangeAttack = true;   
        }
        
        //gameObject.SetActive(false);
    }
}