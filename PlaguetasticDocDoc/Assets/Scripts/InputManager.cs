﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    [Header("Getting Player Touches")]
    Camera cam;
    Vector2 firstPressPos;
    Vector2 secondPressPos;
    Vector2 currentSwipe;
    Vector2 mouseWorldPosition;
    [SerializeField] Vector3 screenBound;
    float currentCamPos;

    [Header("Player State")]
    public GameObject playerMain;
    public PlayerManager playerManager;
    public enum PlayerState { intro, idle, moving, attacking, damaged };
    public PlayerState currentStateType;
    public bool facingLeft;

    [Header("Game State")]
    public bool gameStarted;
    public bool gamePaused;
    public bool playerIsDead;
    public bool canSwipe;

    [Header("Attack Manager")]
    public float attackTimer;
    public float baseAttackTimer;
    public bool canCloseAttack;
    public bool canRangeAttack;


    void Start()
    {
        attackTimer = baseAttackTimer;
        cam = Camera.main;
        playerMain = GameObject.FindGameObjectWithTag("Player");
        playerManager = playerMain.GetComponent<PlayerManager>();

        //Comentar essas linhas quando tiver um GameManager
        gameStarted = true;
    }

    void Update()
    {
        if (!playerIsDead)
        {
            if (gameStarted && !gamePaused)
            {
                if (currentStateType == PlayerState.idle)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        RegisterFirstTouch();
                        CheckIfTouchedOnLeftSide();
                    }

                    if (Input.GetMouseButtonUp(0))
                    {
                        if (canSwipe)
                        {
                            Swipe();
                        }

                        if (currentStateType == PlayerState.idle)
                        {
                            MobileAttack();
                        }

                        attackTimer = baseAttackTimer;
                    }

                    if (Input.GetMouseButton(0))
                    {
                        if (attackTimer > 0)
                        {
                            attackTimer -= Time.deltaTime;
                        }
                    }

                    KeyboardInput();
                    AttackInput();
                }
            }
        }
    }

    public void RegisterFirstTouch()
    {
        mouseWorldPosition = cam.ScreenToWorldPoint(Input.mousePosition);

        firstPressPos = new Vector2(mouseWorldPosition.x, mouseWorldPosition.y);
    }

    public void CheckIfTouchedOnLeftSide()
    {
        screenBound = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        currentCamPos = Camera.main.transform.parent.transform.position.x;

        float midPoint = screenBound.x - currentCamPos;
        float leftPoint = currentCamPos - midPoint;

        if (firstPressPos.x < currentCamPos && firstPressPos.x > leftPoint)
        {
            canSwipe = true;
        }
        else
        {
            canSwipe = false;
        }
    }


    public void Swipe()
    {
        mouseWorldPosition = cam.ScreenToWorldPoint(Input.mousePosition);

        secondPressPos = new Vector2(mouseWorldPosition.x, mouseWorldPosition.y);

        currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

        currentSwipe.Normalize();

        //swipe up
        if (currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
        {
            MoveUp();
        }

        //swipe down
        if (currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
        {
            MoveDown();
        }

        //swipe right
        if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
        {
            MoveRight();
        }

        //swipe left
        if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
        {
            MoveLeft();
        }

        currentSwipe.x = 0;
        currentSwipe.y = 0;
    }

    public void KeyboardInput()
    {


        if (Input.GetKey(KeyCode.UpArrow))
        {
            MoveUp();
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            MoveDown();
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            MoveRight();
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            MoveLeft();
        }
    }

    public void MoveUp()
    {
        playerManager.MoveUp();
    }

    public void MoveDown()
    {
        playerManager.MoveDown();
    }

    public void MoveRight()
    {
        playerManager.MoveRight();
    }

    public void MoveLeft()
    {
        playerManager.MoveLeft();
    }

    public void AttackInput()
    {
        if (canCloseAttack)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                playerManager.Attack();
            }
        }
        
        if (canRangeAttack)
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                playerManager.SecAttack();
            }
        }
    }

    public void MobileAttack()
    {
        //No Swipe
        if (currentSwipe.x > -0.5f && currentSwipe.x < 0.5f && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
        {
            if (canCloseAttack)
            {
                if (attackTimer > 0)
                {
                    playerManager.Attack();
                }
            }
            
            if (canRangeAttack)
            {
                if (attackTimer < 0)
                {
                    playerManager.SecAttack();
                }
            }
        }
    }
}