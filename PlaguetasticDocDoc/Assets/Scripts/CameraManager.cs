﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public delegate void FinishMoveCamera(int newScreen);
    public static event FinishMoveCamera OnFinishedMove;
    
    public delegate void ScreenEvents(int newScreen);
    public static event ScreenEvents OnChangedScreen;
    
    public Vector3 futurePos; //+29.4f;
    public float speed;
    public Vector3 currentPos;

    public bool moveCam;
    public bool moveCamRight;
    public bool moveCamLeft;
    public bool moveCamUp;
    public bool moveCamDown;

    public PlayerManager playerManager;
    public StageManager stageManager;

    public int newScreen;

    void Start()
    {
        playerManager = FindObjectOfType<PlayerManager>();
        stageManager = FindObjectOfType<StageManager>();
    }

    void Update()
    {
        if (moveCam)
        {
            transform.position = Vector3.MoveTowards(transform.position, futurePos, speed * Time.deltaTime);

            if (transform.position.x == futurePos.x && moveCamRight)
            {
                FinishMovingCam();
                StopMovingRight();
            }
            
            if (transform.position.x == futurePos.x && moveCamLeft)
            {
                FinishMovingCam();
                StopMovingLeft();
            }
            
            if (transform.position.y == futurePos.y && moveCamUp)
            {
                FinishMovingCam();
                StopMovingUp();
            }
            
            if (transform.position.y == futurePos.y && moveCamDown)
            {
                FinishMovingCam();
                StopMovingDown();
            }
        }
    }

    public void MoveCameraRight()
    {
        newScreen += 1;
        moveCamRight = true;
        moveCam = true;
        futurePos.x = transform.position.x + 29.4f;
    }
    
    public void MoveCameraLeft()
    {
        newScreen -= 1;
        moveCamLeft = true;
        moveCam = true;
        futurePos.x = transform.position.x - 29.4f;
    }
    
    public void MoveCameraUp()
    {
        newScreen += 10;
        moveCamUp = true;
        moveCam = true;
        futurePos.y = transform.position.y + 17.35f;
    }
    
    public void MoveCameraDown()
    {
        newScreen -= 10;
        moveCamDown = true;
        moveCam = true;
        futurePos.y = transform.position.y - 17.35f;
    }

    // ReSharper disable Unity.PerformanceAnalysis
    public void FinishMovingCam()
    {
        moveCam = false;
        stageManager.CallPlayerIntro();
        OnFinishedMove?.Invoke(newScreen);
        OnChangedScreen?.Invoke(newScreen);
    }

    public void StopMovingRight()
    {
        playerManager.SetPlayerPos(1, 2, -17f, -0.71f);
        stageManager.IntroPlayerHor(-5.14f);
        moveCamRight = false;
    }
    
    public void StopMovingLeft()
    {
        playerManager.SetPlayerPos(4, 2, 17f, -0.71f);
        stageManager.IntroPlayerHor(5.14f);
        moveCamLeft = false;
    }
    
    public void StopMovingUp()
    {
        playerManager.SetPlayerPos(2, 1, -1.7f, -10f);
        stageManager.IntroPlayerVer(- 2.42f);
        moveCamUp = false;
    }
    
    public void StopMovingDown()
    {
        playerManager.SetPlayerPos(2, 3, -1.7f, 10f);
        stageManager.IntroPlayerVer(0.71f);
        moveCamDown = false;
    }
}
