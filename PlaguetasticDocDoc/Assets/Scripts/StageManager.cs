﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : MonoBehaviour
{
    public PlayerManager playerManager;
    public InputManager inputManager;
    public GameObject nextAreaButton;

    void Start()
    {
        playerManager = FindObjectOfType<PlayerManager>();
        inputManager = FindObjectOfType<InputManager>();
    }

    public void CallPlayerIntro()
    {
        inputManager.currentStateType = InputManager.PlayerState.intro;
        inputManager.canSwipe = false;
        inputManager.enabled = false;
        //playerManager.tutorial.showTutorial = true;
    }

    public void TurnButtonOff()
    {
        nextAreaButton.SetActive(false);
    }

    public void IntroPlayerHor(float offset)
    {
        playerManager.introManagement.moveIntroHor = true;
        playerManager.introManagement.moveIntroHorOffset = offset;
    }

    public void IntroPlayerVer(float offset)
    {
        playerManager.introManagement.moveIntroVer = true;
        playerManager.introManagement.moveIntroVerOffset = offset;
    }
}