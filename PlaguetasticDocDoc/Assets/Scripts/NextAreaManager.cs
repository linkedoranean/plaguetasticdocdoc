using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextAreaManager : MonoBehaviour
{
    public GameObject nextRight;
    public GameObject nextLeft;
    public GameObject nextUp;
    public GameObject nextDown;

    public GameObject mainButtons;

    void Start()
    {
        EnemiesScreenManager.OnDisplayButtons += TurnWhichButtonsOn;
        PlayerManager.OnDisplayButtons += TurnButtonsOn;
    }
    
    void OnDestroy()
    {
        EnemiesScreenManager.OnDisplayButtons -= TurnWhichButtonsOn;
        PlayerManager.OnDisplayButtons -= TurnButtonsOn;
    }

    public void TurnWhichButtonsOn(bool right, bool left, bool up, bool down)
    {
        nextRight.SetActive(right);
        nextLeft.SetActive(left);
        nextUp.SetActive(up);
        nextDown.SetActive(down);
    }

    public void TurnButtonsOn()
    {
        mainButtons.SetActive(true);
    }
}