using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesScreenManager : MonoBehaviour
{
    public delegate void ScreenClean();
    public static event ScreenClean OnScreenCleared;
    
    public delegate void WhichButtonsToDisplay(bool right, bool left, bool up, bool down);
    public static event WhichButtonsToDisplay OnDisplayButtons;
    
    public int iD;

    public float enemiesStartupDelay;

    public GameObject[] screenEnemies;

    public bool visited;

    public bool right;
    public bool left;
    public bool up;
    public bool down;
    //public bool tutorialScreen;

//Adicionar ouvir um evento do canvas que dispara após o tutorial

//adicionar ouvir um evento vindo do gerenciador de troca de tela



    void Start()
    {
        PlayerManager.OnFinishedIntro += SetupScreen;
        EnemyHealthManager.OnDefeated += CheckAllEnemies;
    }
    
    void OnDestroy()
    {
        PlayerManager.OnFinishedIntro -= SetupScreen;
        EnemyHealthManager.OnDefeated -= CheckAllEnemies;
    }

    public void SetupScreen(int id)
    {
        if (id == iD)
        {
            Debug.Log("How many [function] called?");
        
            StartCoroutine(StartupEnemies());
        }
    }
    
    IEnumerator StartupEnemies()
    {
        Debug.Log("How many [coroutine] called?");
        
        WaitForSeconds wait = new WaitForSeconds(enemiesStartupDelay);
        foreach (GameObject enemy in screenEnemies)
        {
            enemy.SetActive(true);
            yield return wait;
        }

        CheckIfVisited();
    }

    public void CheckAllEnemies()
    {
        foreach (GameObject enemy in screenEnemies)
        {
            if (enemy.activeSelf)
            {
                break;
            }

            visited = true;
            OnScreenCleared?.Invoke();
            OnDisplayButtons?.Invoke(right, left, up, down);
        }
    }

    public void CheckIfVisited()
    {
        if (visited || screenEnemies.Length == 0)
        {
            OnScreenCleared?.Invoke();
            OnDisplayButtons?.Invoke(right, left, up, down);
        }
    }
}