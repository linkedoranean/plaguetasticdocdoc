using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthManager : MonoBehaviour
{
    public delegate void EnemyEvents();
    public static event EnemyEvents OnDefeated;

    public int enemyHealth;
    
    void Start()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("playerAttack"))
        {
            enemyHealth -= other.GetComponent<PlayerAttack>().damageOutput;
            CheckEnemyCondition();
        }
    }

    public void CheckEnemyCondition()
    {
        if (enemyHealth <= 0)
        {
            Debug.Log("Gritando que morreu");
            this.gameObject.SetActive(false);
            OnDefeated?.Invoke();
        }
    }
}