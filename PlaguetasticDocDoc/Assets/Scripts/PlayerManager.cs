﻿using System;
using System.Collections;
using System.IO.MemoryMappedFiles;
using UnityEngine;

[System.Serializable]
public class PlayerMovement
{
    public float speedVer;
    public float speedHor;
    public int currentX;
    public int currentY;
    public float newHor;
    public float newVer;
    public float previousPos;

    public bool canMoveUp;
    public bool canMoveDown;
    public bool canMoveLeft;
    public bool canMoveRight;
    public Vector3 newPlayerPos;
}

[System.Serializable]
public class IdleManagement
{
    public bool idle;
    public bool attacking;
    public bool gettingHit;
    public bool introing;
    public int selectedSprite;
    public float animTimer;

}

[System.Serializable]
public class IntroManagement
{
    public Camera mainCam;
    public Vector2 introPos;
    public float futurePos;
    public float startingPos;
    public float introSpeed;
    public bool moveIntroHor;
    public float moveIntroHorOffset;
    public bool moveIntroVer;
    public float moveIntroVerOffset;
    public Vector2 screenBounds;

    public float animTimer;
    public int selectedSprite;

    public bool displayChangeScreen;
    public int nextScreen;
}

public class PlayerManager : MonoBehaviour
{
    public delegate void FinishedIntro(int newScreen);
    public static event FinishedIntro OnFinishedIntro;
    
    public delegate void CanDisplayButtons();
    public static event CanDisplayButtons OnDisplayButtons;

    [ContextMenu("Debug Player Hitted")]
    void DebugHItted()
    {
        StartCoroutine(PlayerHitted(true));
    }

    public enum PrimaryAttack { cane };
    public PrimaryAttack currentPrimary;

    public enum SecondaryAttack { throwCane };
    public SecondaryAttack currentSecondary;

    public int playerLife;
    public bool playerIsDead;

    [Header("Player Components")]
    public Rigidbody playerRB;

    [Header("Input Manager comms")]
    public InputManager inputManager;

    [Header("Idle Manager")]
    public IdleManagement idleManagement;

    [Header("Intro Manager")]
    public IntroManagement introManagement;

    [Header("Player Positions / Limits")]
    public PlayerMovement playerMovement;

    [Header("Player Animations")]
    public GameObject playerSpriteMain;
    public SpriteRenderer playerSprite;
    public float animationSpeed;
    public Sprite[] idleSprite;
    public Sprite[] walkSprite;
    public Sprite[] hittedSprites;
    public Sprite[] teleporteSprites;
    public Sprite[] deathSprites;
    public Sprite[] caneAttack;
    public Sprite[] throwCaneAttack;

    [Header("Close Cane Attack")]
    public GameObject caneAttackObj;

    [Header("Throw Cane Attack")]
    public GameObject throwCaneAttackObj;

    [Header("Tutorial Manager")]
    public TutorialManager tutorial;

    void Start()
    {
        EnemiesScreenManager.OnScreenCleared += AttackTutorialManager;
        CameraManager.OnFinishedMove += CanChangeScreen;
        
        playerRB = GetComponent<Rigidbody>();
        inputManager = FindObjectOfType<InputManager>();
        playerSprite = playerSpriteMain.GetComponent<SpriteRenderer>();

        introManagement.mainCam = Camera.main;
        GetScreenBounds();
        transform.position = new Vector3((introManagement.mainCam.transform.parent.transform.position.x) - 17f, transform.position.y, transform.position.z);
        //StartCoroutine(PlayerIntro());

        ChangeSelectedCanvas();
    }

    private void OnDestroy()
    {
        EnemiesScreenManager.OnScreenCleared -= AttackTutorialManager;
        CameraManager.OnFinishedMove -= CanChangeScreen;
    }

    private void Update()
    {
        CheckIdle();

        if (idleManagement.idle)
        {
            if (idleManagement.animTimer >= 0)
            {
                idleManagement.animTimer -= Time.deltaTime;
            }

            if (idleManagement.animTimer < 0)
            {
                playerSprite.sprite = idleSprite[idleManagement.selectedSprite];
                idleManagement.selectedSprite++;

                if (idleManagement.selectedSprite >= idleSprite.Length)
                {
                    idleManagement.selectedSprite = 0;
                }

                idleManagement.animTimer = 0.1f;
            }
        }

        if (!idleManagement.idle)
        {
            idleManagement.selectedSprite = 0;
            idleManagement.animTimer = 0.1f;
        }
    }

    void FixedUpdate()
    {
        if (playerMovement.canMoveUp || playerMovement.canMoveDown)
        {
            playerMovement.newPlayerPos = transform.position;
            playerMovement.newPlayerPos.y = Mathf.MoveTowards(transform.position.y, playerMovement.newVer, playerMovement.speedVer * Time.deltaTime);

            playerRB.MovePosition(playerMovement.newPlayerPos);

            if (transform.position.y == playerMovement.newVer)
            {
                inputManager.currentStateType = InputManager.PlayerState.idle;
                playerMovement.canMoveUp = false;
                playerMovement.canMoveDown = false;
            }
        }

        if (playerMovement.canMoveLeft || playerMovement.canMoveRight)
        {
            playerMovement.newPlayerPos = transform.position;
            playerMovement.newPlayerPos.x = Mathf.MoveTowards(transform.position.x, playerMovement.newHor, playerMovement.speedHor * Time.deltaTime);

            playerRB.MovePosition(playerMovement.newPlayerPos);

            if (transform.position.x == playerMovement.newHor)
            {
                inputManager.currentStateType = InputManager.PlayerState.idle;
                playerMovement.canMoveLeft = false;
                playerMovement.canMoveRight = false;
            }
        }

        IntroMovement();
    }

    // ReSharper disable Unity.PerformanceAnalysis
    public void IntroMovement()
    {
        if (introManagement.moveIntroHor)
        {
            introManagement.futurePos = (introManagement.mainCam.transform.parent.transform.position.x) + introManagement.moveIntroHorOffset;//introManagement.screenBounds.x - 17.57764f;

            introManagement.introPos.x = Mathf.MoveTowards(transform.position.x, introManagement.futurePos, introManagement.introSpeed * Time.deltaTime);
            introManagement.introPos.y = transform.position.y;

            playerRB.MovePosition(introManagement.introPos);

            PlayerIntro();

            CheckIntro();
        }

        if (introManagement.moveIntroVer)
        {
            introManagement.futurePos = (introManagement.mainCam.transform.parent.transform.position.y) + introManagement.moveIntroVerOffset;//introManagement.screenBounds.x - 17.57764f;

            introManagement.introPos.x = transform.position.x;
            introManagement.introPos.y = Mathf.MoveTowards(transform.position.y, introManagement.futurePos, introManagement.introSpeed * Time.deltaTime);

            playerRB.MovePosition(introManagement.introPos);

            PlayerIntro();

            CheckIntro();
        }
    }

    // ReSharper disable Unity.PerformanceAnalysis
    private void CheckIntro()
    {
        if (transform.position.x == introManagement.futurePos ||
            transform.position.y == introManagement.futurePos)
        {
            introManagement.moveIntroHor = false;
            introManagement.moveIntroVer = false;
            inputManager.currentStateType = InputManager.PlayerState.idle;
            playerMovement.canMoveLeft = false;
            playerMovement.canMoveRight = false;

            idleManagement.selectedSprite = 0;
            idleManagement.animTimer = 0.1f;

            inputManager.currentStateType = InputManager.PlayerState.idle;

            if (introManagement.displayChangeScreen)
            {
                CallScreenMovement(introManagement.nextScreen);
            }

            DisplayTutorial();
        }
    }

    private void CallScreenMovement(int nextScreen)
    {
        OnFinishedIntro?.Invoke(nextScreen);
        introManagement.displayChangeScreen = false;
    }

    private void CanChangeScreen(int screenIdToCall)
    {
        introManagement.displayChangeScreen = true;
        introManagement.nextScreen = screenIdToCall;
    }

    private void DisplayTutorial()
    {
        if (tutorial.showTutorial)
        {
            Debug.Log("Quantas chamou?");
            tutorial.tutorialAnim.SetTrigger("ShowLeft");
            tutorial.textManager.nextText();
        }

        if (!tutorial.showTutorial)
        {
            if (tutorial.selectedCanvas < tutorial.introCanvas.Length)
            {
                inputManager.enabled = false;
            }

            if (tutorial.selectedCanvas == tutorial.introCanvas.Length)
            {
                inputManager.enabled = true;
            }
        }
    }

    public void GetScreenBounds()
    {
        introManagement.screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
    }

    public void PlayerIntro()
    {
        if (introManagement.animTimer >= 0)
        {
            introManagement.animTimer -= Time.deltaTime;
        }

        if (introManagement.animTimer <= 0)
        {
            playerSprite.sprite = walkSprite[introManagement.selectedSprite];
            introManagement.selectedSprite++;

            if (introManagement.selectedSprite >= walkSprite.Length)
            {
                introManagement.selectedSprite = 0;
            }

            introManagement.animTimer = 0.1f;
        }
    }
    
    public void SetPlayerPos(int newX, int newY, float xOffset, float yOffset)
    {
        var position = introManagement.mainCam.transform.parent.transform.position;
        transform.position = new Vector3((position.x) + xOffset, (position.y) + yOffset, transform.position.z);

        playerMovement.currentX = newX;
        playerMovement.currentY = newY;
    }

    public void MoveUp()
    {
        if (playerMovement.currentY < 3)
        {
            playerMovement.canMoveUp = true;
            playerMovement.newVer = transform.position.y + 1.71f;
            MovementManager();

            playerMovement.currentY++;
        }
    }

    public void MoveDown()
    {
        if (playerMovement.currentY > 0)
        {
            playerMovement.canMoveDown = true;
            playerMovement.newVer = transform.position.y - 1.71f;
            MovementManager();

            playerMovement.currentY--;
        }
    }

    public void MoveLeft()
    {
        if (playerMovement.currentX > 0)
        {
            playerMovement.canMoveLeft = true;
            //playerMovement.previousPos = transform.position.x - (2 * 3.44f);
            playerMovement.previousPos = transform.position.x;
            playerMovement.newHor = transform.position.x - 3.44f;
            MovementManager();

            playerMovement.currentX--;
        }
    }

    public void MoveRight()
    {
        if (playerMovement.currentX < 5)
        {
            playerMovement.canMoveRight = true;
            playerMovement.previousPos = transform.position.x;
            playerMovement.newHor = transform.position.x + 3.44f;
            MovementManager();

            playerMovement.currentX++;
        }
    }

    public void MoveDamaged(bool defaultDirection)
    {
        if (defaultDirection && playerMovement.currentX > 0)
        {
            playerMovement.canMoveLeft = true;
            playerMovement.newHor = playerMovement.previousPos;
            MovementManager();

            playerMovement.currentX--;
        }
        
        if (!defaultDirection && playerMovement.currentX < 5)
        {
            playerMovement.canMoveRight = true;
            playerMovement.newHor = playerMovement.previousPos;
            MovementManager();

            playerMovement.currentX++;
        }
    }

    public void CheckIdle()
    {
        if (playerMovement.canMoveDown ||
            playerMovement.canMoveLeft ||
            playerMovement.canMoveRight ||
            playerMovement.canMoveUp ||
            idleManagement.attacking ||
            idleManagement.gettingHit ||
            introManagement.moveIntroHor||
            introManagement.moveIntroVer)
        {
            idleManagement.idle = false;
        }

        else
        {
            idleManagement.idle = true;
        }
    }

    public void Attack()
    {
        idleManagement.attacking = true;
        inputManager.currentStateType = InputManager.PlayerState.attacking;
        StartCoroutine(MainAttack());
    }
    
    public void SecAttack()
    {
        idleManagement.attacking = true;
        inputManager.currentStateType = InputManager.PlayerState.attacking;
        StartCoroutine(AltAttack());
    }

    public void MovementManager()
    {
        //playerMovement.previousPos = transform.position.x;
        inputManager.currentStateType = InputManager.PlayerState.moving;
        StartCoroutine(MoveAnimation());

        if (tutorial.countStep)
        {
            tutorial.stepCounter++;

            if (tutorial.stepCounter == 4)
            {
                tutorial.stepCounter = 0;
                inputManager.enabled = false;
                tutorial.countStep = false;
                tutorial.showAttackTutorial = true;
                OnDisplayButtons?.Invoke();
                //tutorial.nextButton.SetActive(true);
            }
        }
    }

    public void ChangeSelectedCanvas()
    {
        if (tutorial.selectedCanvas < tutorial.introCanvas.Length)
        {
            tutorial.textManager = tutorial.introCanvas[tutorial.selectedCanvas].GetComponent<TextManager>();
            tutorial.tutorialAnim = tutorial.introCanvas[tutorial.selectedCanvas].GetComponent<Animator>();
            tutorial.selectedCanvas++;
        }
    }

    public void AttackTutorialManager()
    {
        OnDisplayButtons?.Invoke();
        //tutorial.nextButton.SetActive(true);
        
        //MUDAR PARA PERMITIR QUANDO O INIMIGO FOR DESTRUIDO
        /*if (tutorial.showAttackTutorial)
        {
            tutorial.stepCounter++;

            if (tutorial.stepCounter == 2)
            {
                inputManager.enabled = false;
                tutorial.countStep = false;
                tutorial.showAttackTutorial = false;
                tutorial.nextButton.SetActive(true);
            }
        }*/
    }

    public void ButtonsShouldBeDisplayed(bool right, bool left, bool up, bool down)
    {
        
    }

    IEnumerator MoveAnimation()
    {
        WaitForSeconds wait = new WaitForSeconds(animationSpeed);
        foreach (Sprite moveSprite in teleporteSprites)
        {
            playerSprite.sprite = moveSprite;
            yield return wait;
        }

        playerSprite.sortingOrder = playerMovement.currentY * -1;
        //playerSprite.sprite = idleSprite;
    }

    IEnumerator MainAttack()
    {
        //AttackTutorialManager();

        if (currentPrimary == PrimaryAttack.cane)
        {
            WaitForSeconds wait = new WaitForSeconds(0.1f);
            foreach (Sprite attackSprite in caneAttack)
            {
                playerSprite.sprite = attackSprite;

                if (playerSprite.sprite == caneAttack[2])
                {
                    caneAttackObj.SetActive(true);
                }

                if (playerSprite.sprite == caneAttack[3])
                {
                    caneAttackObj.SetActive(false);
                }
                yield return wait;
            }

        }
        idleManagement.attacking = false;
        //playerSprite.sprite = idleSprite;
        inputManager.currentStateType = InputManager.PlayerState.idle;
    }

    IEnumerator AltAttack()
    {
        //AttackTutorialManager();

        WaitForSeconds wait = new WaitForSeconds(0.1f);
        throwCaneAttackObj.SetActive(true);
        foreach (Sprite attackSprite in throwCaneAttack)
        {
            playerSprite.sprite = attackSprite;
            yield return wait;
        }

        idleManagement.attacking = false;
        //playerSprite.sprite = idleSprite;
        inputManager.currentStateType = InputManager.PlayerState.idle;
    }

    IEnumerator PlayerBlinking()
    {
        for (int i = 0; i < 15; i++)
        {
            Color temp = playerSprite.color;

            temp.a = 0;
            playerSprite.color = temp;

            yield return new WaitForSeconds(0.1f);

            temp.a = 1;
            playerSprite.color = temp;

            yield return new WaitForSeconds(0.1f);
        }

        gameObject.GetComponent<BoxCollider>().enabled = true;
    }
    
    IEnumerator PlayerHitted(bool defaultDirection)
    {
        idleManagement.gettingHit = true;
        //MoveLeft();
        MoveDamaged(defaultDirection);
        inputManager.currentStateType = InputManager.PlayerState.damaged;

        WaitForSeconds wait = new WaitForSeconds(0.05f);
        foreach (Sprite hitted in hittedSprites)
        {
            playerSprite.sprite = hitted;

            yield return wait;
        }

        yield return new WaitForSeconds(0.1f);

        idleManagement.gettingHit = false;

        StartCoroutine(PlayerBlinking());
        //playerSprite.sprite = idleSprite;
        inputManager.currentStateType = InputManager.PlayerState.idle;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!playerIsDead)
        {
            if (other.tag == "enemyAttack" ||
                other.tag == "enemy")
            {
                if (playerLife > 0)
                {
                    playerLife--;
                    StopAllCoroutines();
                    caneAttackObj.SetActive(false);
                    //throwCaneAttackObj.SetActive(false);
                    inputManager.currentStateType = InputManager.PlayerState.damaged;
                    gameObject.GetComponent<BoxCollider>().enabled = false;
                    Debug.Log("Acertou jogador");
                }

                if (playerLife == 0)
                {
                    playerIsDead = true;
                }
            }
            
            if (other.tag == "barrier")
            {
                if (playerLife > 0)
                {
                    playerLife--;
                    StopAllCoroutines();
                    caneAttackObj.SetActive(false);
                    inputManager.currentStateType = InputManager.PlayerState.damaged;
                    Debug.Log("Acertou jogador");
                }

                if (playerLife == 0)
                {
                    playerIsDead = true;
                }
            }
            
            if (other.transform.position.x > transform.position.x)
            {
                StartCoroutine(PlayerHitted(true));
            }
            if (other.transform.position.x < transform.position.x)
            {
                StartCoroutine(PlayerHitted(false));
            }
        }
    }
}