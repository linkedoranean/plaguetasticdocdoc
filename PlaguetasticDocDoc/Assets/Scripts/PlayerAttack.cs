﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public enum AttackType { cane, throwCane };
    public AttackType currentType;

    public GameObject playerMain;
    public PlayerManager playerManager;

    public int damageOutput;

    public Vector3 attackPos;

    void Awake()
    {
        playerMain = GameObject.FindGameObjectWithTag("Player");
        playerManager = playerMain.GetComponent<PlayerManager>();
    }

    void OnEnable()
    {
        transform.position = new Vector3(playerMain.transform.position.x + 3.41f, playerMain.transform.position.y, playerMain.transform.position.z);
    }

    void Start()
    {
        
    }

    void Update()
    {
        
    }
}