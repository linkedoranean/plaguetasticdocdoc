﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TutorialManager
{
    public int stepCounter;
    public bool showTutorial;
    public bool countStep;
    public bool showAttackTutorial;
    public bool showLongAttackTutorial;
    public bool showTutorialEnding;


    //[TextArea] public string[] attackTexts;
    //[TextArea] public string[] longAttackTexts;
    //[TextArea] public string[] tutorialEndingTexts;

    public Animator tutorialAnim;
    public string tutorialTriggerShow;
    public string tutorialTriggerEnd;

    public GameObject[] introCanvas;
    public TextManager textManager;
    public GameObject nextButton;

    public int selectedCanvas;
}