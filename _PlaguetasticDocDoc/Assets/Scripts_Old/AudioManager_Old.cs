﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public enum Scene {MainMenu, Stage}
    public Scene scene;

    public string musicStatus;
    public string sfxStatus;

    public AudioSource stageMusic;
    public AudioSource[] sfxFiles;

    public bool pauseAudio;

    void Awake()
    {

    }

    void Start ()
    {
		
	}
	
    void Update ()
    {

    }
}