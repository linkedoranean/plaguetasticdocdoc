﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    public bool pausedGame;
    public bool endStage;

    public enum Platform { Mobile, Desktop };
    public Platform selectedPlatform;

    public GameObject mobileCanvas;
    public GameObject consoleCanvas;

    public enum Scene { MainMenu, Stage }
    public Scene scene;

    public int sceneToLoad;
    public int nextStage;
    public int sceneToContinue;

    /*
        public int nextStage;

        public List<GameObject> enemiesOnScreen = new List<GameObject>();
    */
    public GameObject introCanvas;
    public Animator introAnim;

    public PlayerManagerB playerManager;
    public Animator playerAnim;

    public Button desktopContinueButton;
    public Button desktopPauseButton;
    public GameObject pauseButtom;

    public GameObject[] enemyManagers;
    public int selectedEnemyManager;


    public GameObject continueCanvas;
    Animator continueAnim;
    public float continueTimer;

    public Animator bossAnim;

    public GameObject endCanvas;
    public Animator endCanvasAnim;

    public GameObject bossCanvas;
    public Animator bossCanvasAnim;

    /*
    public AudioManager audioManager;

    public GameObject playerController;
    public GameObject playerManager;
    public int playerHealth;



    public int checkHealth;
    public int firstTimePlaying;
*/
    void Awake()
    {
        if (Application.platform == RuntimePlatform.Android ||
            Application.platform == RuntimePlatform.IPhonePlayer)
        {
            selectedPlatform = Platform.Mobile;
        }
        if (Application.platform == RuntimePlatform.WindowsPlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            selectedPlatform = Platform.Desktop;
        }
/*
        if (scene == Scene.MainMenu)
        {
            Debug.Log("" + PlayerPrefs.GetInt("firstTime"));

            if (PlayerPrefs.GetInt("firstTime") != 0 && PlayerPrefs.GetInt("firstTime") != 1)
            {
                PlayerPrefs.SetInt("firstTime", 0);
            }


            if (PlayerPrefs.GetInt("firstTime") == 0)
            {
                PlayerPrefs.SetInt("playerHealth", 5);
                PlayerPrefs.SetInt("musicStatus", 1);
                PlayerPrefs.SetInt("sfxStatus", 1);
                PlayerPrefs.SetInt("CurrentStage", 1);
                PlayerPrefs.SetInt("firstTime", 1);
            }

            if (selectedPlatform == Platform.Mobile)
            {
                PlayerPrefs.SetString("platform", "mobile");
            }

            if (selectedPlatform == Platform.Desktop)
            {
                PlayerPrefs.SetString("platform", "desktop");
            }
        }
        if (scene == Scene.Stage)
        {
            if (PlayerPrefs.GetString("platform") == "mobile")
            {
                selectedPlatform = Platform.Mobile;
            }

            if (PlayerPrefs.GetString("platform") == "desktop")
            {
                selectedPlatform = Platform.Desktop;
            }
        }
*/
    }

    void Start ()
    {
        if (selectedPlatform == Platform.Mobile)
        {
            mobileCanvas.SetActive (true);
            consoleCanvas.SetActive(false);
        }

        if (selectedPlatform == Platform.Desktop)
        {
            mobileCanvas.SetActive(false);
            consoleCanvas.SetActive(true);
        }
        /*
                audioManager = GetComponent<AudioManager>();

                if (scene == Scene.Stage)
                {
                    continueAnim = continueCanvas.GetComponent<Animator>();
                    playerAnim = playerManager.GetComponent<Animator>();
                    playerController = GameObject.FindGameObjectWithTag("Player");
                    checkHealth = PlayerPrefs.GetInt("playerHealth");
                    PlayerPrefs.SetInt("CurrentStage", sceneToContinue);
                }
                if (scene == Scene.MainMenu)
                {
                    sceneToContinue = PlayerPrefs.GetInt("CurrentStage");
                    playerHealth = PlayerPrefs.GetInt("playerHealth");
                }
        */
        continueAnim = continueCanvas.GetComponent<Animator>();
    }
	
	void Update ()
    {
        if (introAnim != null && playerAnim != null)
        {
            if (introAnim.GetCurrentAnimatorStateInfo(0).IsName("outIntro") &&
                playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Canvas"))
            {
                playerAnim.SetTrigger("StartGame");
                introCanvas.SetActive(false);
                //playerManager.playerMove = true;
            }
        }

        if (selectedEnemyManager < enemyManagers.Length)
        {
            if (enemyManagers[selectedEnemyManager].GetComponent<EnemyManagerB>().enemiesActive == false)
            {
                if (selectedEnemyManager < enemyManagers.Length - 1)
                {
                    Debug.Log("A");
                    playerAnim.SetTrigger("ChangeStage");
                    introCanvas.SetActive(true);
                    introAnim.SetTrigger("ChangeStage");
                    selectedEnemyManager++;
                    playerAnim.ResetTrigger("Restart");
                }
            }
        }

        if (introAnim.GetCurrentAnimatorStateInfo(0).IsName("ShowStage"))
        {
            Debug.Log("B");
            playerAnim.SetTrigger("Restart");
            introAnim.ResetTrigger("ChangeStage");
            playerAnim.ResetTrigger("ChangeStage");
            if (selectedEnemyManager < enemyManagers.Length)
            {
                enemyManagers[selectedEnemyManager].SetActive(true);
            }
            gameObject.transform.parent.transform.position = new Vector3(0, 0, 0);
            gameObject.transform.parent.transform.parent.transform.position = new Vector3(0, 0, 0);
        }

        if (selectedEnemyManager == enemyManagers.Length)
        {
            Debug.Log("Iniciou o fim da fase");
            //if (playerAnim.GetCurrentAnimatorStateInfo(0).IsName("LoadNext"))
            //{
               // SceneManager.LoadScene(nextStage);
            //}
            playerAnim.SetTrigger("EndStage");
            selectedEnemyManager++;
        }
        /*
                if (scene == Scene.Stage)
                {
                    for (int i = 0; i < enemiesOnScreen.Count; i++)
                    {
                        if (enemiesOnScreen[i].GetComponent<EnemyBehavior>().enemyHealth <= 0)
                        {
                            enemiesOnScreen.RemoveAt(i);
                            //enemiesOnScreen.Sort();
                        }
                    }


                    if (!GameObject.FindGameObjectWithTag("enemyManager").GetComponent<EnemyManager>().enabled &&
                        enemiesOnScreen.Count == 0)
                    {
                        continueAnim.SetTrigger("continue");
                        playerAnim.SetTrigger("nextStage");
                    }
                    if (continueAnim.GetCurrentAnimatorStateInfo(0).IsName("GameOff"))
                    {

                    }

                    if (playerManager.transform.position.x > 11)
                    {
                        continueAnim.SetTrigger("fadeOut");
                    }

                    if (playerController.transform.position.x > 15)
                    {
                        SceneManager.LoadScene(nextStage);
                    }
                }
        */

        if (Input.GetKeyDown(KeyCode.Escape) && introCanvas.activeSelf == false)
        {
            if (pausedGame == true)
            {
                pausedGame = false;
                if (desktopContinueButton != null)
                {
                    desktopContinueButton.onClick.Invoke();
                }
            }
            else if (pausedGame == false)
            {
                pausedGame = true;
                if (desktopPauseButton != null)
                {
                    desktopPauseButton.onClick.Invoke();
                }
            }   
        }

        if (introCanvas != null)
        {
            if (introCanvas.activeSelf == false && Time.timeScale == 1)
            {
                pauseButtom.SetActive(true);
            }
            else
            {
                pauseButtom.SetActive(false);
            }
        }

        ///GO ON MANAGER
        if (!endStage)
        {
            if (continueAnim.GetCurrentAnimatorStateInfo(0).IsName("GameOn") &&
                playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
            {
                if (continueTimer > 0)
                {
                    continueTimer -= Time.deltaTime;
                }

                if (continueTimer <= 0)
                {
                    continueAnim.SetTrigger("continue");
                }
            }

            if (continueAnim.GetCurrentAnimatorStateInfo(0).IsName("Continue") ||
                !playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
            {
                continueTimer = 5;
            }
        }

        ///FINISH ANIM
        if (endStage == true && bossAnim.GetCurrentAnimatorStateInfo(0).IsName("Dead_OFF"))
        {
            endCanvas.SetActive(true);

            if (endCanvasAnim.GetCurrentAnimatorStateInfo(0).IsName("LoadNextStage"))
            {
                SceneManager.LoadScene(nextStage);
            }
        }

        if (bossCanvas.activeSelf)
        {
            playerManager.playerMove = false;
        }
        if (bossCanvasAnim.GetCurrentAnimatorStateInfo(0).IsName("Boss01_End"))
        {
            bossCanvas.SetActive(false);
        }
        if (!bossCanvas.activeSelf)
        {
            playerManager.playerMove = true;
        }
    }

    public void PauseGame()
    {
        //pausedGame = true;
        Time.timeScale = 0;
        //audioManager.pauseAudio = true;

    }

    public void UnpauseGame()
    {
        Time.timeScale = 1;
        //pausedGame = false;
        //audioManager.pauseAudio = false;
    }

    public void QuitGame()
    {
        if (scene == Scene.MainMenu)
        {
            Application.Quit();
        }
        if (scene == Scene.Stage)
        {
            UnpauseGame();
            SceneManager.LoadScene(0);
        }
    }

    public void LoadScene()
    {
        UnpauseGame();
        SceneManager.LoadScene(sceneToLoad);
    }

    public void GameStart()
    {
        //PlayerPrefs.SetInt("playerHealth", 5);
        //PlayerPrefs.SetInt("CurrentStage", 1);
        //SceneManager.LoadScene(sceneToContinue);

    }

    public void RetryGame()
    {
        //PlayerPrefs.SetInt("playerHealth", 5);
        SceneManager.LoadScene(sceneToContinue);
    }
    /*
        public void MuteMusic()
        {
            PlayerPrefs.SetInt("musicStatus", 0);
            audioManager.stageMusic.volume = PlayerPrefs.GetInt("musicStatus");
        }

        public void UnmuteMusic()
        {
            PlayerPrefs.SetInt("musicStatus", 1);
            audioManager.stageMusic.volume = PlayerPrefs.GetInt("musicStatus");
        }

        public void MuteSFX()
        {
            PlayerPrefs.SetInt("sfxStatus", 0);
            foreach (AudioSource audioSouce in audioManager.sfxFiles)
            {
                audioSouce.volume = PlayerPrefs.GetInt("sfxStatus");
            }
        }

        public void UnmuteSFX()
        {
            PlayerPrefs.SetInt("sfxStatus", 1);
            foreach (AudioSource audioSouce in audioManager.sfxFiles)
            {
                audioSouce.volume = PlayerPrefs.GetInt("sfxStatus");
            }
        }

        public void GameContinue()
        {
            PlayerPrefs.SetInt("playerHealth", playerHealth);
            SceneManager.LoadScene(sceneToContinue);
        }

        public void ResetGame()
        {
            PlayerPrefs.SetInt("playerHealth", 5);
            PlayerPrefs.SetInt("musicStatus", 1);
            PlayerPrefs.SetInt("sfxStatus", 1);
            PlayerPrefs.SetInt("CurrentStage", 1);
            PlayerPrefs.SetInt("firstTime", 1);
        }
    */
}