﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashManager : MonoBehaviour
{
    public Animator splashAnim;

    void Start()
    {
        
    }

    void Update()
    {
        if (splashAnim.GetCurrentAnimatorStateInfo(0).IsName("LoadScene"))
        {
            SceneManager.LoadScene(1);
        }
    }
}
