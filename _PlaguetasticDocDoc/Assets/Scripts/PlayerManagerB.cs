﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManagerB : MonoBehaviour
{
    //CONTROLE DE DEIXAR SPRITE BRANCA
    public SpriteRenderer myRenderer;
    private Shader shaderGUItext;
    private Shader shaderSpritesDefault;
    //

    public int playerX;
    public int playerY;
    public int health;

    public Animator childAnim;

    public bool playerMove;
    public bool playerMoveBack;
    public bool playerMoveForward;

    public int selectedWeapon;
    public int blinkCounter;

    public float counter;

    public GameObject attackParent;
    public GameObject introCanvas;
    public GameObject cameraManager;
    public Animator cameraAnim;
    public Animator gameOverAnim;

    Vector2 firstPressPos;
    Vector2 secondPressPos;
    Vector2 currentSwipe;

    int buttonMask;

    float camRayLenght = 10000f;

    public float goSpeed;
    public float newX;

    public BoxCollider playerCollider;

    void Start()
    {
        //CONTROLE DE DEIXAR SPRITE BRANCA
        shaderGUItext = Shader.Find("GUI/Text Shader");
        shaderSpritesDefault = Shader.Find("Sprites/Default");
        //

        childAnim = gameObject.transform.GetChild(0).transform.GetComponent<Animator>();

        buttonMask = LayerMask.GetMask("Attack");
    }

    void Update()
    {
        newX = transform.position.x;

        if (Time.timeScale == 1)
        {
            if (playerMove)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    Attack();
                }

                Swipe();

                Keyboard();
            }
            
        }

        //PLAYER DEATH MANAGER
        if (health <= 0)
        {
            childAnim.ResetTrigger("NotDead");
            childAnim.SetTrigger("Dead");
            gameOverAnim.SetTrigger("flash");
        }

        if (health > 0 && childAnim.GetCurrentAnimatorStateInfo(0).IsName("HitBack") ||
            health > 0 && childAnim.GetCurrentAnimatorStateInfo(0).IsName("Hitted"))
        {
            childAnim.SetTrigger("NotDead");
        }

        if (childAnim.GetCurrentAnimatorStateInfo(0).IsName("GameOver"))
        {
            gameOverAnim.SetTrigger("gameOver");
        }

        //PLAYER DEATH MANAGER

        if (childAnim.GetCurrentAnimatorStateInfo(0).IsName("ChangeStage"))
        {
            newX += goSpeed;

            transform.localPosition = new Vector3(newX, transform.localPosition.y, transform.localPosition.z);

            transform.GetChild(0).GetComponent<BoxCollider>().enabled = false;
        }
        if (childAnim.GetCurrentAnimatorStateInfo(0).IsName("Intro"))
        {
            transform.localPosition = new Vector3(-5.14f, transform.localPosition.y, transform.localPosition.z);
            playerX = 0;
            transform.GetChild(0).GetComponent<BoxCollider>().enabled = true;
        }

        if (childAnim.GetCurrentAnimatorStateInfo(0).IsName("CoolDown"))
        {
            childAnim.ResetTrigger("Throw");
            childAnim.ResetTrigger("ThrowBack");
        }

        if (childAnim.GetCurrentAnimatorStateInfo(0).IsName("Canvas"))
        {
            introCanvas.SetActive(true);
        }

        if (childAnim.GetCurrentAnimatorStateInfo(0).IsName("HitBack") ||
            childAnim.GetCurrentAnimatorStateInfo(0).IsName("Hitted"))
        {
            playerCollider.enabled = false;
            childAnim.ResetTrigger("HitBack");
            childAnim.ResetTrigger("Hit");
        }
        if (childAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            playerCollider.enabled = true;
        }

        if (Input.GetMouseButtonDown(0))
        {
            AttackRaycast();
        }
        /*
        if (childAnim.GetCurrentAnimatorStateInfo(0).IsName("WalkForward"))
        {
            childAnim.ResetTrigger("Walk");
        }
        */
    }

    void CameraControllerRight()
    {
        if (playerMoveForward)
        {
            if (cameraManager.transform.position.x < 60f)
            {
                cameraAnim.SetTrigger("MoveForward");
                cameraManager.transform.position = new Vector3(cameraManager.transform.position.x + 17f, 0, 0);
            }
        }
    }
    void CameraControllerLeft()
    {
        if (playerMoveBack)
        {
            cameraAnim.SetTrigger("MoveBack");
            cameraManager.transform.position = new Vector3(cameraManager.transform.position.x - 17f, 0, 0);
        }
    }

    public void MoveUp()
    {
        if (childAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            if (playerY < 2)
            {
                childAnim.SetTrigger("WalkUp");
                playerY++;
                myRenderer.sortingOrder++;
                transform.position = new Vector3(transform.position.x, transform.position.y + 1.71f, transform.position.z);
            }
        }
    }
    public void MoveDown()
    {
        if (childAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            if (playerY > -2)
            {
                childAnim.SetTrigger("WalkDown");
                playerY--;
                myRenderer.sortingOrder--;
                transform.position = new Vector3(transform.position.x, transform.position.y - 1.71f, transform.position.z);
            }
        }
    }
    public void MoveRight()
    {
        if (childAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            if (playerX < 19)
            {
                childAnim.SetTrigger("Walk");
                playerX++;
                transform.position = new Vector3(transform.position.x + 3.46f, transform.position.y, transform.position.z);

                if (playerX > 3 && playerX < 17 && cameraManager.transform.position.x < 60f)
                {
                    CameraControllerRight();
                }
            }
        }
    }
    public void MoveLeft()
    {
        if (childAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            if (playerX > -1)
            {
                childAnim.SetTrigger("WalkBack");
                playerX--;
                transform.position = new Vector3(transform.position.x - 3.46f, transform.position.y, transform.position.z);
                CameraControllerLeft();
            }
        }
    }

    public void PlayerHitted()
    {
        if (transform.position.x > -7)
        {
            childAnim.SetTrigger("HitBack");
            transform.position = new Vector3(transform.position.x - 3.46f, transform.position.y, transform.position.z);
            playerX--;
        }
        if (transform.position.x < -8)
        {
            childAnim.SetTrigger("Hit");
        }

        health = health - 1;
    }

    //ATTACK MANAGER
    public void AttackRaycast()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit buttonHit;

        if (Physics.Raycast(camRay, out buttonHit, camRayLenght, buttonMask))
        {
            if (playerMove)
            {
                Attack();
            }
        }
    }


    //CONTROLE DE DEIXAR SPRITE BRANCA
    public void whiteSprite()
    {
        myRenderer.material.shader = shaderGUItext;
        myRenderer.color = Color.white;
    }

    public void normalSprite()
    {
        myRenderer.material.shader = shaderSpritesDefault;
        myRenderer.color = Color.white;
    }
    //
    
    //CONTROLE DE MOVIMENTAÇÃO POR TECLADO
    public void Keyboard()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            MoveUp();
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            MoveDown();
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            MoveLeft();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            MoveRight();
        }
    }
    //

    //CONTROLE DE MOVIMENTAÇÃO POR SWIPE
    public void Swipe()
    {
        if (Input.touches.Length > 0)
        {
            Touch t = Input.GetTouch(0);

            if (t.phase == TouchPhase.Began &&
                t.position.x > 0 && t.position.x < 900 &&
                t.position.y > 0 && t.position.y < 700)
            {
                firstPressPos = new Vector2(t.position.x, t.position.y);
            }

            if (t.phase == TouchPhase.Ended)
            {
                if (t.position.x < 900 && t.position.y < 700)
                {
                    secondPressPos = new Vector2(t.position.x, t.position.y);

                    currentSwipe = new Vector3(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

                    currentSwipe.Normalize();

                    //swipe up
                    if (currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
                    {
                        MoveUp();
                    }
                    //swipe down
                    if (currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
                    {
                        MoveDown();
                    }
                    //swipe left
                    if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
                    {
                        MoveLeft();
                    }
                    //swipe right
                    if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
                    {
                        MoveRight();
                    }
                }
            }
        }
    }
    //

    //CONTROLE DO ATAQUE
    public void Attack()
    {
        if (childAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            if (selectedWeapon == 0)
            {
                attackParent.transform.GetChild(selectedWeapon).gameObject.SetActive(true);
                childAnim.SetTrigger("AttackClose");
            }

            if (selectedWeapon == 1)
            {
                attackParent.transform.GetChild(selectedWeapon).gameObject.SetActive(true);
                childAnim.SetTrigger("Throw");
            }
        }
    }
    //
    /*
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "enemy")
        {
            PlayerHitted();
        }
    }
    */
    void OnTriggerStay(Collider other)
    {
        if (other.name == "MoveBackManager")
        {
            playerMoveBack = true;
        }
        if (other.name == "MoveForwardManager")
        {
            playerMoveForward = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        playerMoveBack = false;
        playerMoveForward = false;
    }
}