﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManagerB : MonoBehaviour
{
    public GameObject[] enemies;

    public bool enemiesActive;

    void Start()
    {
        
    }

    void Update()
    {
        for (int i = 0; i < enemies.Length; i++)
        {
            if (enemies[i].activeInHierarchy)
            {
                enemiesActive = true;
                break;
            }
            else
            {
                enemiesActive = false;
            }
        }
    }
}
