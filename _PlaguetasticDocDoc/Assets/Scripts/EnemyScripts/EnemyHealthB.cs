﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthB : MonoBehaviour
{
    public int health;

    public bool bossMarker;

    public GameManager gameManager;

    public GameObject bossCanvas;

    void Start()
    {
        if (bossMarker)
        {
            bossCanvas.SetActive(true);
        }
    }

    void Update()
    {
        if (bossMarker)
        {
            if (health <= 0)
            {
                gameManager.selectedEnemyManager++;
                gameManager.endStage = true;
            }
        }
    }
}
