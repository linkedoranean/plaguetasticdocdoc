﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehavior_Projectile : MonoBehaviour
{
    public Animator parentAnim;
    public Animator cameraAnim;
    public Animator playerAnim;

    public Vector3 enemyPos;
    public float verticalValue; //1.62f
    public float horizontalValue; //3.39f

    public BoxCollider attackBoxCollider;

    /// <summary>
    public bool changeInt;

    public int currentAnim;

    public string[] triggers;

    public float tempPosX;
    public float tempPosY;
    /// </summary>

    /// PROJECTILE ATTACK MANAGER
    public GameObject attackObj;
    public bool attack;

    void Start()
    {
        attackBoxCollider = GetComponent<BoxCollider>();
        parentAnim = gameObject.transform.parent.GetComponent<Animator>();
        playerAnim = GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>();
    }

    void Update()
    {
        if (playerAnim.GetCurrentAnimatorStateInfo(0).IsName("GameOver"))
        {
            parentAnim.enabled = false;
        }
        gameObject.transform.parent.transform.localPosition = enemyPos;

        if (playerAnim != null)
        {
            if (playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
            {
                gameObject.transform.parent.GetComponent<Animator>().enabled = true;
            }
        }

        if (cameraAnim == null)
        {
            cameraAnim = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Animator>();
        }

        if (parentAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            attackBoxCollider.enabled = true;
        }

        if (parentAnim.GetCurrentAnimatorStateInfo(0).IsName("Dead_OFF"))
        {
            gameObject.transform.parent.transform.parent.gameObject.SetActive(false);
        }

        if (parentAnim.isActiveAndEnabled && parentAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            if (changeInt == true)
            {
                parentAnim.ResetTrigger("MoveUp");
                parentAnim.ResetTrigger("MoveDown");
                parentAnim.ResetTrigger("MoveLeft");
                parentAnim.ResetTrigger("MoveRight");

                Debug.Log("All triggers resetted");

                if (currentAnim < triggers.Length)
                {
                    currentAnim++;
                }

                if (currentAnim == triggers.Length)
                {
                    currentAnim = 0;
                }
                tempPosX = gameObject.transform.parent.transform.localPosition.x;
                tempPosY = gameObject.transform.parent.transform.localPosition.y;

                Debug.Log("Definiu próxima anim");
                changeInt = false;
            }
        }

        if (parentAnim.GetCurrentAnimatorStateInfo(0).IsName("ReadyMove"))
        {
            attack = true;

            if (triggers[currentAnim] == "MoveUp")
            {
                enemyPos.x = tempPosX;
                enemyPos.y = tempPosY + verticalValue;
                gameObject.transform.position = new Vector3(gameObject.transform.position.x,
                                                            gameObject.transform.position.y - 0.79f,
                                                            gameObject.transform.position.z);

                gameObject.transform.parent.transform.localPosition = enemyPos;
            }

            if (triggers[currentAnim] == "MoveDown")
            {
                enemyPos.x = tempPosX;
                enemyPos.y = tempPosY - verticalValue;
                gameObject.transform.position = new Vector3(gameObject.transform.position.x,
                                                            gameObject.transform.position.y - 0.79f,
                                                            gameObject.transform.position.z);

                gameObject.transform.parent.transform.localPosition = enemyPos;
            }

            if (triggers[currentAnim] == "MoveLeft")
            {
                enemyPos.x = tempPosX - horizontalValue;
                enemyPos.y = tempPosY;
                gameObject.transform.position = new Vector3(gameObject.transform.position.x,
                                                            gameObject.transform.position.y - 0.79f,
                                                            gameObject.transform.position.z);

                gameObject.transform.parent.transform.localPosition = enemyPos;
            }

            if (triggers[currentAnim] == "MoveRight")
            {
                enemyPos.x = tempPosX + horizontalValue;
                enemyPos.y = tempPosY;
                gameObject.transform.position = new Vector3(gameObject.transform.position.x,
                                                            gameObject.transform.position.y - 0.79f,
                                                            gameObject.transform.position.z);

                gameObject.transform.parent.transform.localPosition = enemyPos;
            }

            parentAnim.SetTrigger(triggers[currentAnim]);
            Debug.Log("Ativou Anim e selecionou anim");

            changeInt = true;
        }

        if (parentAnim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            if (attack == true)
            {
                attackObj.SetActive(true);
                attack = false;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.transform.parent.GetComponent<BoxCollider>().enabled = false;
            other.transform.parent.GetComponent<PlayerManagerB>().PlayerHitted();
            cameraAnim.SetTrigger("screenShake");
            attackBoxCollider.enabled = false;
        }
    }
}