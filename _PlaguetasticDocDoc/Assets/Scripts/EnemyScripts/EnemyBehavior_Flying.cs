﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehavior_Flying : MonoBehaviour
{
    public Animator parentAnim;
    public Animator cameraAnim;
    public Animator playerAnim;

    public BoxCollider attackBoxCollider;

    public BoxCollider attackBox;

    public GameObject parent;

    public AnimatorClipInfo[] m_CurrentClipInfo; 

    void Start()
    {
        attackBoxCollider = GetComponent<BoxCollider>();
        parentAnim = gameObject.transform.parent.GetComponent<Animator>();
        playerAnim = GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>();
    }

    void Update()
    {
        //

        if (playerAnim.GetCurrentAnimatorStateInfo(0).IsName("GameOver"))
        {
            parentAnim.enabled = false;
        }

        if (playerAnim != null)
        {
            if (playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
            {
                parentAnim.SetTrigger("StartLoop");
            }
        }

        if (cameraAnim == null)
        {
            cameraAnim = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Animator>();
        }

        if (parentAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle_Low") ||
            parentAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle_High"))
        {
            attackBoxCollider.enabled = true;
        }

        if (parentAnim.GetCurrentAnimatorStateInfo(0).IsName("Dead_OFF"))
        {
            gameObject.transform.parent.transform.parent.gameObject.SetActive(false);
        }

        if (parentAnim.isActiveAndEnabled && parentAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle_Low") ||
            parentAnim.isActiveAndEnabled && parentAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle_High"))
        {
            attackBox.enabled = true;
        }

        if (parentAnim.GetCurrentAnimatorStateInfo(0).IsName("AttackLow") ||
            parentAnim.GetCurrentAnimatorStateInfo(0).IsName("AttackHigh"))
        {
            parentAnim.ResetTrigger("Attack");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.transform.parent.GetComponent<BoxCollider>().enabled = false;
            other.transform.parent.GetComponent<PlayerManagerB>().PlayerHitted();
            cameraAnim.SetTrigger("screenShake");
            attackBoxCollider.enabled = false;
        }
    }
}