﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFixer : MonoBehaviour
{
    public GameObject mainCamera;

    public Animator playerAnim;

    public Animator cameraAnim;

    void Start()
    {
        
    }

    void Update()
    {
        if (mainCamera.transform.position.x <0)
        {
            mainCamera.transform.position = new Vector3(0, 0, 0);
        }

        if (mainCamera.transform.position.x > 55f)
        {
            mainCamera.transform.position = new Vector3(55f, 0, 0);
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.name == "PlayerController")
        {
            if (playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle") && cameraAnim.GetCurrentAnimatorStateInfo(0).IsName("idleState"))
            {
                if (other.transform.position.x <= 16.9f)
                {
                    mainCamera.transform.position = new Vector3(0, 0, 0);
                }
                if (other.transform.position.x > 17 && other.transform.position.x <= 33.99f)
                {
                    mainCamera.transform.position = new Vector3(17, 0, 0);
                }
                if (other.transform.position.x > 34 && other.transform.position.x <= 50.99f)
                {
                    mainCamera.transform.position = new Vector3(34, 0, 0);
                }
                if (other.transform.position.x > 51 && other.transform.position.x <= 68)
                {
                    mainCamera.transform.position = new Vector3(51, 0, 0);
                }
            }
        }
    }
}