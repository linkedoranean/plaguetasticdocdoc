﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack_Projectile : MonoBehaviour
{
    public float goSpeed;
    public float limit;
    public float newX;
    public float x;

    public bool go;

    public Animator canvasAnim;

    public GameObject newParent;

    void Start()
    {
        
    }

    void OnEnable()
    {
        newParent = gameObject.transform.parent.gameObject;
        gameObject.transform.position = new Vector3(newParent.transform.position.x, newParent.transform.position.y, newParent.transform.position.z);
        gameObject.transform.parent = null;
        newX = x;
        go = true;
        gameObject.GetComponent<Animator>().enabled = true;
        gameObject.GetComponent<BoxCollider>().enabled = true;
        gameObject.GetComponent<SpriteRenderer>().enabled = true;
    }

    void Update()
    {
        if (canvasAnim == null)
        {
            canvasAnim = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Animator>();
        }

        if (transform.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("RatAttack") && Time.timeScale == 1)
        {
            go = true;
        }

        if (transform.localPosition.x > limit && go == true && Time.timeScale == 1)
        {
            newX += goSpeed;

            transform.position = new Vector3(newX, transform.position.y, transform.position.z);
        }
        if (transform.localPosition.x < limit && go == true && Time.timeScale == 1)
        {
            //transform.GetComponent<Animator>().SetTrigger("Back");
            gameObject.GetComponent<BoxCollider>().enabled = false;
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            transform.parent = newParent.transform;
            gameObject.transform.localPosition = new Vector3(newParent.transform.localPosition.x, newParent.transform.localPosition.y, newParent.transform.localPosition.z);
            go = false;
            gameObject.SetActive(false);
        }

        if (go == false && Time.timeScale == 1)
        {
            gameObject.GetComponent<BoxCollider>().enabled = false;
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            transform.parent = newParent.transform;
            gameObject.transform.localPosition = new Vector3(newParent.transform.localPosition.x, newParent.transform.localPosition.y, newParent.transform.localPosition.z);
            gameObject.SetActive(false);
        }
    }

    void OnDisable()
    {
        //newX = 0;
        //gameObject.transform.localPosition = new Vector3(0, 0, 0);
        //transform.localPosition = new Vector3(0, -0.25f, 0);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.transform.parent.GetComponent<PlayerManagerB>().PlayerHitted();
            go = false;
            if (transform.localPosition.x < 1 && go == false)
            {
                gameObject.GetComponent<BoxCollider>().enabled = false;
                gameObject.GetComponent<SpriteRenderer>().enabled = false;
                transform.parent = newParent.transform;
                gameObject.transform.localPosition = new Vector3(newParent.transform.localPosition.x, newParent.transform.localPosition.y, newParent.transform.localPosition.z);
                gameObject.SetActive(false);
            }
            canvasAnim.SetTrigger("screenShake");
        }
    }
}