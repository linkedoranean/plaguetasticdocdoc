﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackBManager : MonoBehaviour
{
    public int attackPower;

    public float goSpeed;
    public float backSpeed;
    public float limit;
    public float newX;

    public bool go;

    public PlayerManagerB playerManager;

    public Animator playerAnim;

    public Animator canvasAnim;

    void Start()
    {
        
    }

    void OnEnable()
    {
        newX = 0;
        transform.localPosition = new Vector3(0.96f, 0, 0);
        go = true;
        playerManager.playerMove = false;
    }

    void Update()
    {
        canvasAnim = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Animator>();

        if (transform.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("AttackB_Loop") && Time.timeScale == 1)
        {
            if (transform.localPosition.x < limit && go == true)
            {
                newX += goSpeed;

                transform.localPosition = new Vector3(newX, transform.localPosition.y, transform.localPosition.z);
            }
            if (transform.localPosition.x > limit && go == true)
            {
                go = false;
            }

            if (transform.localPosition.x > 0 && go == false)
            {
                newX -= backSpeed;
                transform.localPosition = new Vector3(newX, transform.localPosition.y, transform.localPosition.z);
            }

            if (transform.localPosition.x < 0)
            {
                playerManager.playerMove = true;
                transform.localPosition = new Vector3(0, transform.localPosition.y, transform.localPosition.z);
                this.gameObject.SetActive(false);
            }

            if (transform.localPosition.x > 1 && transform.localPosition.x < 1.5f && go == false)
            {
                playerAnim.SetTrigger("ThrowBack");
            }
        }
    }

    void OnDisable()
    {
        newX = 0;
        transform.localPosition = new Vector3(0.96f, 0, 0);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "enemy")
        {
            go = false;
            if (transform.localPosition.x < 1 && go == false)
            {
                playerAnim.SetTrigger("ThrowBack");
            }
            canvasAnim.SetTrigger("enemyShake");

            other.GetComponent<EnemyHealthB>().health = other.GetComponent<EnemyHealthB>().health - attackPower;

            if (other.GetComponent<EnemyHealthB>().health < 1)
            {
                other.gameObject.transform.parent.GetComponent<Animator>().SetTrigger("Dead");
            }
        }
    }
}