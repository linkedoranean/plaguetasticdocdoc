﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackAManager : MonoBehaviour
{
    public int attackPower;

    public PlayerManagerB playerManager;

    public Animator playerAnim;

    public Animator canvasAnim;

    void Start()
    {

    }

    void Update()
    {
        canvasAnim = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Animator>();

        if (transform.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("AttackA_Loop") && Time.timeScale == 1)
        {
            this.gameObject.SetActive(false);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "enemy")
        {
            canvasAnim.SetTrigger("enemyShake");
            other.GetComponent<EnemyHealthB>().health = other.GetComponent<EnemyHealthB>().health - attackPower;

            if (other.GetComponent<EnemyHealthB>().health < 1)
            {
                other.gameObject.transform.parent.GetComponent<Animator>().SetTrigger("Dead");
            }
        }
    }
}