﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack_CloseAttack : MonoBehaviour
{
    public Animator parentAnim;
    public Animator cameraAnim;

    public BoxCollider attackBoxCollider;

    void Start()
    {
        attackBoxCollider = GetComponent<BoxCollider>();
        parentAnim = gameObject.transform.parent.transform.parent.GetComponent<Animator>();
    }

    void Update()
    {
        if (parentAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle")) 
        {
            attackBoxCollider.enabled = true;
        }
        if (parentAnim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            parentAnim.ResetTrigger("Attack");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            //cameraAnim.SetTrigger("screenShake");
            parentAnim.SetTrigger("Attack");
            attackBoxCollider.enabled = false;
        }
    }
}